#include <iostream>
#include "..\include\sampler.h"

const STRING querys[] = {"SELECT * FROM Win32_NetworkAdapterConfiguration WHERE MACAddress IS NOT NULL"};
const STRING headers[] = {"Servicename", "MACAddress", "IPAddress","DNSDomain", "DefaultIPGateway"};
const wchar_t* columns[] = { L"ServiceName", L"MACAddress", L"IPAddresss",L"DNSDomain", L"DefaultIPGateways"};

int main()
{
	std::cout << "Starting" << std::endl;
	IEnumWbemClassObject* new_pEnumerator = NULL;
	new_pEnumerator = get_wmi_penum(querys[0]);
	
    IWbemClassObject* pclsObj = NULL;
    ULONG uReturn = 0;
    
    while (new_pEnumerator)
    {
        HRESULT hr = new_pEnumerator->Next(WBEM_INFINITE, 1,
            &pclsObj, &uReturn);

        if (0 == uReturn)
        {
            break;
        }
        VARIANT vtProp;
        for (int i = 0; i < (sizeof(columns) / sizeof(*columns)); i++)
        {
            vtProp.bstrVal = NULL;
            // Get the value of the Name property
            hr = pclsObj->Get(columns[i], 0, &vtProp, 0, 0);
            std::wstring stringVal(vtProp.bstrVal, SysStringLen(vtProp.bstrVal));
            wcout << headers[i] << ": " << stringVal << endl;
            
        }
        wcout << "-----------" << endl;
        VariantClear(&vtProp);
        pclsObj->Release();
        
        
    }
    // Cleanup
    // ========

    new_pEnumerator->Release();
    CoUninitialize();

    // Program successfully completed.
	std::cin.get();
    return 0;
}

